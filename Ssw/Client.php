<?php

namespace SSW;

class Client {
  // Config properties
  protected $_service_domain = 'growave.io';
  protected $_scheme = 'https://';
  protected $_api_path = '/api/';
  protected $_authorize_path = 'authorize';
  protected $_access_token_path = 'access_token';

  protected $api_url;
  protected $authorize_url;
  protected $access_token_url;

  // App defined properties
  protected $consumer_key;
  protected $consumer_secret;
  protected $redirect_uri;


  protected $token;

  /**
   * @param $consumer_key
   * @param $consumer_secret
   */
  public function __construct($consumer_key, $consumer_secret, $redirect_uri = null)
  {
    $this->consumer_key = $consumer_key;
    $this->consumer_secret = $consumer_secret;
    $this->redirect_uri = $redirect_uri;

    $this->api_url = $this->_scheme . $this->_service_domain . $this->_api_path;
    $this->authorize_url = $this->api_url . $this->_authorize_path;
    $this->access_token_url = $this->api_url . $this->_access_token_path;
  }

  // Authorization Methods
  /**
   * Returns the authorization link
   *
   * @param string $hesid
   * @return string
   * @throws \Exception
   */
  public function getAuthorizationUri($hesid)
  {
    if(!$this->redirect_uri)
      throw new \Exception('redirect_uri is not defined');

    $authorizationUri = $this->authorize_url . '?' . http_build_query(array(
        'response_type' => 'code',
        'state' => substr(md5(time()), 0, 3),
        'client_id' => $this->consumer_key,
        'redirect_uri' => $this->redirect_uri
      ));
    return $authorizationUri;
  }

  /**
   * Authorize with grant type "client_credentials"
   *
   * @param bool|false $scopes
   * @return mixed
   * @throws \Exception
   */
  public function getAppToken($scopes = false)
  {
    $payload = array(
      'client_id' => $this->consumer_key,
      'client_secret' => $this->consumer_secret,
      'scope' => $scopes,
      'grant_type' => 'client_credentials',
    );

    if (!$scopes) {
      $payload['scope'] = 'read_user write_user read_wishlist write_wishlist read_review write_review read_gallery read_reward write_reward app_install';
    }

    $params = array(
      'uri' => $this->access_token_url,
      'payload' => $payload,
      'method' => 'POST',
    );

    $request = new Request($params);
    $response = $request->curlHttpApiRequest();
    $response = json_decode($response, true);
    $this->token = isset($response['access_token']) ? $response['access_token'] : '';

    return $response;
  }

  /**
   * Authorize with grant type "authorization_code"
   *
   * @param string $code
   * @param bool|false $scopes
   * @return mixed
   * @throws \Exception
   */
  public function getAccessToken($code, $scopes = false)
  {
    if(!$this->redirect_uri)
      throw new \Exception('redirect_uri is not defined');

    $payload = array(
      'code' => $code,
      'client_id' => $this->consumer_key,
      'client_secret' => $this->consumer_secret,
      'scope' => $scopes,
      'redirect_uri' => $this->redirect_uri,
      'grant_type' => 'authorization_code'
    );

    if (!$scopes) {
      $payload['scope'] = 'read_user';
    }

    $params = array(
      'uri' => $this->access_token_url,
      'payload' => $payload,
      'method' => 'POST',
    );

    $request = new Request($params);
    $response = $request->curlHttpApiRequest();
    $response = json_decode($response, true);
    $this->token = isset($response['access_token']) ? $response['access_token'] : '';

    return $response;
  }

  /**
   * Set access token
   *
   * @param $access_token
   */
  public function setAccessToken($access_token)
  {
    $this->token = $access_token;
  }

  /**
   * Api Call
   *
   * @param $method
   * @param $path
   * @param array $params
   * @return mixed
   * @throws \Exception
   */
  public function call($method, $path, array $params = array())
  {
    if (is_null($path)) {
      throw new \InvalidArgumentException('You must specify a request url.');
    } elseif (!is_string($path)) {
      throw new \InvalidArgumentException('String expected, got ' . gettype($path));
    }
    $query = in_array($method, array('GET', 'DELETE')) ? $params : array();
    $payload = in_array($method, array('POST', 'PUT')) ? json_encode($params) : array();

    $query['access_token'] = $this->token;
    $request_headers = in_array($method, array('POST', 'PUT')) ? array("Content-Type: application/json; charset=utf-8", 'Expect:') : array();

    // add auth headers
    $request_headers[] = 'X-SSW-Access-Token: ' . $this->token;
    $requestParams = array(
      'method' => strtoupper($method),
      'uri' => $this->api_url . ltrim($path, '/'),
      'query' => $query,
      'payload' => $payload,
      'request_headers' => $request_headers
    );

    $request = new Request($requestParams);
    $response = $request->curlHttpApiRequest();
    $response = json_decode($response, true);

    $last_response_headers = $request->getLastResponseHeaders();

    if (isset($response['errors']) or ($last_response_headers['http_status_code'] >= 400))
      throw new ApiException($method, $path, $params, $last_response_headers, $response);
    return $response;
  }

  // REST For Users

  /**
   * Returns the authorized user
   *
   * @return mixed
   * @throws \Exception
   */
  public function getUserProfile()
  {
    return $this->call('GET', 'user/me');
  }

  /**
   * Returns the lis of users
   *
   * @param int $limit
   * @param int $page
   * @return array
   * @throws \Exception
   */
  public function getUsers($limit = 250, $page = 1)
  {
    return $this->call('GET', 'users', array(
      'limit' => $limit,
      'page' => $page
    ));
  }

  /**
   * Search for user by email
   *
   * @param $email
   * @return mixed
   * @throws \Exception
   */
  public function searchUser($email)
  {
    return $this->call('GET', 'users/search', array(
      'field' => 'email',
      'value' => $email
    ));
  }

  /**
   * Returns the user with given user_id
   *
   * @param $user_id
   * @return mixed
   * @throws \Exception
   */
  public function getUser($user_id)
  {
    return $this->call('GET', 'users/' . $user_id);
  }

  /**
   * Edit the user with $user_id
   *
   * @param $user_id
   * @param $user_data
   * @return mixed
   * @throws \Exception
   */
  public function editUser($user_id, $user_data)
  {
    return $this->call('PUT', 'users/' . $user_id, $user_data);
  }

  // REST For Wishlist

  /**
   * Receive a list of all Products in wishlist
   *
   * @param int $user_id
   * @param int $board_id
   * @param int $limit
   * @param int $page
   * @return array
   * @throws \Exception
   */
  public function getWishlist($user_id = 0, $board_id = 0, $limit = 250, $page = 1)
  {
    return $this->call('GET', 'wishlist', array(
      'user_id' => $user_id,
      'board_id' => $board_id,
      'limit' => $limit,
      'page' => $page
    ));
  }

  /**
   * Add product to wishlist
   *
   * @param $user_id
   * @param $product_id
   * @param int $board_id
   * @return array
   * @throws \Exception
   */
  public function addToWishlist($user_id, $product_id, $board_id = 0)
  {
    return $this->call('POST', 'wishlist', array(
      'user_id' => $user_id,
      'product_id' => $product_id,
      'board_id' => $board_id
    ));
  }

  /**
   * REST For Reviews
   *
   * @param int $user_id
   * @param null $product_id
   * @param int $limit
   * @param int $page
   * @return mixed
   * @throws \Exception
   */
  public function reviews($user_id = 0, $product_id = null, $limit = 250, $page = 1)
  {
    $data = array(
      'user_id' => $user_id,
      'limit' => $limit,
      'page' => $page
    );
    if(!is_null($product_id) ) {
      $data['product_id'] = $product_id;
    }
    return $this->call('GET', 'reviews', $data);
  }

  /**
   * Post review
   *
   * @param string $email
   * @param string $name
   * @param string $body
   * @param int $rate
   * @param int $product_id
   * @return mixed
   * @throws \Exception
   */
  public function postReview($email = '', $name = '', $body = '', $rate = 0, $product_id = 0)
  {
    return $this->call('POST', 'reviews', array(
      'email' => $email,
      'name' => $name,
      'body' => $body,
      'rate' => $rate,
      'product_id' => $product_id
    ));
  }

  /**
   * Get rate off products
   *
   * @param null $product_ids
   * @param int $limit
   * @param int $page
   * @return mixed
   * @throws \Exception
   */
  public function rate($product_ids = null, $limit = 250, $page = 1)
  {
    $data = array(
      'limit' => $limit,
      'page' => $page
    );
    if(!is_null($product_ids)) {
      $data['product_ids'] = $product_ids;
    }
    return $this->call('GET', 'reviews/products', $data);
  }

  /**
   * Returns the Gallery List
   *
   * @param int $limit
   * @param int $page
   *
   * @return array
   * @throws \Exception
   */
  public function getGalleries($limit = 250, $page = 1)
  {
      return $this->call('GET', 'galleries', array(
          'limit' => $limit,
          'page' => $page
      ));
  }

  /**
   * Returns the Media List
   *
   * @param int $productId
   * @param int $galleryId
   * @param int $limit
   * @param int $page
   *
   * @return array
   * @throws \Exception
   */
  public function getMedia($productId, $galleryId = null, $limit = 250, $page = 1)
  {
    return $this->call('GET', 'media', array(
      'product_id' => $productId,
      'gallery_id' => $galleryId,
      'limit' => $limit,
      'page' => $page
    ));
  }

  /**
   * Returns the QA List
   *
   * @param int $itemId
   * @param int $limit
   * @param int $page
   *
   * @return array
   * @throws \Exception
   */
  public function getQA($itemId, $limit = 250, $page = 1)
  {
    return $this->call('GET', 'qa/' . $itemId, array(
      'limit' => $limit,
      'page' => $page
    ));
  }

  /**
   * Earn reward (custom actions)
   *
   * @param int $user_id
   * @param null $email
   * @param string $rule_type
   * @param null $points
   * @return mixed
   * @throws \Exception
   */
  public function earnReward($user_id = 0, $email = null, $rule_type = '', $points = null)
  {
    return $this->call('POST', 'reward/earn', array(
      'user_id' => $user_id,
      'email' => $email,
      'rule_type' => $rule_type,
      'points' => $points
    ));
  }

  /**
   * Get Earning rules
   *
   * @param int $userId
   * @param int $all
   * @return array
   * @throws \Exception
   */
  public function getEarningRules($userId = 0, $all = 0)
  {
    return $this->call('GET', 'reward/earningRules', [
      'user_id' => $userId,
      'all' => $all,
    ]);
  }

  /**
   * Install the Growave app to the current or specified theme
   *
   * @param ?string $themeId
   * @return array
   * @throws \Exception
   */
  public function install($themeId = null)
  {
    $data = [];
    if(!is_null($themeId)) {
      $data['theme_id'] = $themeId;
    }
    return $this->call('POST', 'install', $data);
  }
}

class Request
{
  private $_config = array(
    'uri' => null,
    'method' => null,
    'payload' => '',
    'query' => '',
    'request_headers' => null,
    'last_response_headers' => null,
  );

  public function __construct($config = array())
  {
    if ($config !== null) {
      $this->setConfig($config);
    }
  }

  public function setConfig($config = array())
  {
    if(!is_array($config)) {
      throw new \InvalidArgumentException('Array or Zend_Config object expected, got ' . gettype($config));
    }

    foreach($config as $k => $v) {
      $this->_config[strtolower($k)] = $v;
    }
  }

  public function curlHttpApiRequest()
  {
    $url = $this->curlAppendQuery($this->_config['uri'],$this->_config['query']);
    $ch = curl_init($url);
    $this->curlSetOptions($ch);
    $response = curl_exec($ch);
    $errno = curl_errno($ch);
    $error = curl_error($ch);
    curl_close($ch);
    if ($errno) throw new \Exception($error, $errno);
    list($message_headers, $message_body) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
    $this->_config['last_response_headers'] = $this->curlParseHeaders($message_headers);
    return $message_body;
  }

  private function curlParseHeaders($message_headers)
  {
    $header_lines = preg_split("/\r\n|\n|\r/", $message_headers);
    $headers = array();
    list(, $headers['http_status_code'], $headers['http_status_message']) = explode(' ', trim(array_shift($header_lines)), 3);
    foreach ($header_lines as $header_line)
    {
      list($name, $value) = explode(':', $header_line, 2);
      $name = strtolower($name);
      $headers[$name] = trim($value);
    }

    return $headers;
  }

  public  function curlAppendQuery($url, $query)
  {
    if (empty($query)) return $url;
    if (is_array($query)) return "$url?".http_build_query($query);
    else return "$url?$query";
  }

  public function curlSetOptions($ch)
  {
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_USERAGENT, 'HAC');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );

    curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, $this->_config['method']);
    if(!empty($this->_config['request_headers'])){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->_config['request_headers']);
    }
    if ($this->_config['method'] != 'GET' && !empty($this->_config['payload']))
    {
      if (is_array($this->_config['payload'])) $this->_config['payload'] = http_build_query($this->_config['payload']);
      curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->_config['payload']);
    }
  }

  public function getLastResponseHeaders()
  {
    return $this->_config['last_response_headers'];
  }
}

class ApiException extends \Exception
{
  protected $method;
  protected $path;
  protected $params;
  protected $response_headers;
  protected $response;

  function __construct($method, $path, $params, $response_headers, $response)
  {
    $this->method = $method;
    $this->path = $path;
    $this->params = $params;
    $this->response_headers = $response_headers;
    $this->response = $response;

    parent::__construct($response_headers['http_status_message'], $response_headers['http_status_code']);
  }

  function getMethod() { return $this->method; }
  function getPath() { return $this->path; }
  function getParams() { return $this->params; }
  function getResponseHeaders() { return $this->response_headers; }
  function getResponse() { return $this->response; }
}